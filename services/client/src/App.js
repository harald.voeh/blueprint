import React, { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { ToastContainer } from "react-toastify";

import Home from "./components/Home";
import LoginForm from "./components/LoginForm";
import RegisterForm from "./components/RegisterForm";
import NavBar from "./components/NavBar";
import AddUser from "./components/AddUser";
import UserList from "./components/UserList";
import Status from "./components/Status";
import ProtectedRoute from "./components/common/ProtectedRoute";

import auth from "./services/authService";

import "react-toastify/dist/ReactToastify.css";
import Logout from "./components/Logout";

class App extends Component {
  state = { authToken: null, user: null, users: [] };

  async componentDidMount() {
    const authToken = auth.getCurrentUser();
    const { data: user } = await auth.status();
    this.setState({ authToken, user });
  }

  render() {
    return (
      <React.Fragment>
        <ToastContainer />
        <NavBar user={this.state.user} />
        <div className="container mt-4">
          <Switch>
            <Route path="/login" component={LoginForm} />
            <Route path="/register" component={RegisterForm} />
            <Route path="/logout" component={Logout} />
            <Route path="/status" component={Status} />
            <ProtectedRoute path="/add-user" component={AddUser} />
            <ProtectedRoute path="/user-list" component={UserList} />
            <Route path="/" component={Home} />
          </Switch>
        </div>
      </React.Fragment>
    );
  }
}

export default App;
