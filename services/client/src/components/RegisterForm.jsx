import React from "react";
import Joi from "joi-browser";
import Form from "./common/Form";
import auth from "../services/authService";

class RegisterForm extends Form {
  state = {
    data: {
      username: "",
      email: "",
      password: ""
    },
    errors: {}
  };
  schema = {
    username: Joi.string()
      .required()
      .label("Username"),
    email: Joi.string()
      .email()
      .required()
      .label("Email"),
    password: Joi.string()
      .required()
      .label("Password")
  };

  doSubmit = async () => {
    try {
      await auth.register(this.state.data);
      this.props.history.push("/login");
    } catch (ex) {
      if (ex.response && ex.response.status === 400) {
        const errors = { ...this.state.errors };
        errors.email = ex.response.data.message;
        this.setState({ errors });
      }
    }
  };

  render() {
    return (
      <React.Fragment>
        <h1>Register</h1>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput(
            "username",
            "Username",
            "text",
            "Your username in the app."
          )}
          {this.renderInput(
            "email",
            "Email",
            "email",
            "Please use a valid email."
          )}
          {this.renderInput("password", "Password", "password")}
          {this.renderButton("Register")}
        </form>
      </React.Fragment>
    );
  }
}

export default RegisterForm;
