import React, { Component } from "react";
import { getUsers } from "../services/userService";

class UserList extends Component {
  state = { users: [] };
  async componentDidMount() {
    const { data: users } = await getUsers();
    this.setState({ users });
  }
  render() {
    return (
      <React.Fragment>
        <h1>UserList component</h1>
        <div className="row">
          <div className="col">
            <table className="table">
              <thead className="thead-dark">
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">First</th>
                  <th scope="col">Last</th>
                  <th scope="col">Handle</th>
                </tr>
              </thead>
              <tbody>
                {this.state.users.map(user => (
                  <tr key={user.id}>
                    <th scope="row">{user.id}</th>
                    <td>{user.username}</td>
                    <td>{user.email}</td>
                    <td>@mdo</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default UserList;
