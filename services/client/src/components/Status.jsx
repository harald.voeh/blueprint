import React, { Component } from "react";
import auth from "../services/authService";

class Status extends Component {
  state = { user: null };

  async componentDidMount() {
    try {
      const { data: user } = await auth.status();
      this.setState({ user });
    } catch (ex) {
      console.log(ex);
    }
  }

  render() {
    const { user } = this.state;
    return (
      <React.Fragment>
        <h1>Status</h1>
        {user ? (
          <p>Hi {user.username}, welcome back!</p>
        ) : (
          <p>Please login first.</p>
        )}
      </React.Fragment>
    );
  }
}

export default Status;
