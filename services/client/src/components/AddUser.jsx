import React from "react";
import Form from "./common/Form";
import Joi from "joi-browser";

import { addUser } from "../services/userService";
import { toast } from "react-toastify";

class AddUser extends Form {
  state = {
    data: {
      username: "",
      email: "",
      password: ""
    },
    errors: {}
  };

  schema = {
    username: Joi.string()
      .required()
      .label("Username"),
    email: Joi.string()
      .email()
      .required()
      .label("Email"),
    password: Joi.string()
      .required()
      .label("Password")
  };

  doSubmit = async () => {
    const user = this.state.data;

    try {
      await addUser(user);
      this.props.history.push("/");
    } catch (ex) {
      if (ex.response && ex.response.status === 400) {
        toast.error("This email already exists in our database.");
      }
    }
    console.log("Submitting");
  };

  render() {
    return (
      <React.Fragment>
        <h1>Add User</h1>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput(
            "email",
            "Email",
            "email",
            "Your Email is safe with us."
          )}
          {this.renderInput("username", "Username")}
          {this.renderInput("password", "Password", "password")}
          {this.renderButton("Submit")}
        </form>
      </React.Fragment>
    );
  }
}

export default AddUser;
