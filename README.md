# React-Flask blueprint project

This project runs a flask REST API backend and features user authorisation via JWT. The front end is based on react and bootstrap.

The app runs on heroku: (takes some time to load): https://cryptic-river-07731.herokuapp.com/

![Preview Screenshot](screenshot.png)
